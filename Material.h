#pragma once
#include "AliceGL.h"
#include "Attribute.h"
#include "UniformUpdater.h"
#include "MaterialProperty.h"
#include "RenderState.h"
#include "RenderPass.h"

class Material {
public:
	Material();
	void Bind(Camera* camera);
	void SetMatrix4(const char* uniform_name, float* mat4);
	void SetVec4(const char* uniform_name, float* ptr);
	RenderPass* mBaseRenderPass, *mAdditiveRenderPass;
private:
	GLuint mProgram;
};
