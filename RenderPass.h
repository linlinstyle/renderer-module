#pragma once
#include "AliceGL.h"
#include "Attribute.h"
#include "UniformUpdater.h"
#include "MaterialProperty.h"
#include "RenderState.h"
#include "shader.h"

class RenderPass {
public:
	void Init(const char*vs, const char*fs);
	void Bind(Camera* camera);
	void SetMatrix4(const char* uniform_name, float* mat4);
	void SetVec4(const char* uniform_name, float* ptr);
	void SetSampler2D(const char* uniform_name, GLuint texture_name);
	void SetShader(Shader* shader);
	void AppendUniformUpdater(UniformUpdater* uniform_updater);

	std::unordered_map<std::string, MaterialProperty*> mProperty;
	RenderState mRenderState;
	UniformUpdater* mUniforms;
	Shader* mShader;
};
