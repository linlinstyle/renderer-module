#pragma once
#include "LinkList.h"
#include "UniformUpdater.h"

class Material;
class VBO;
class IBO;

enum RenderCatalog {
	kRenderCatalogForward,
	kRenderCatalogDefferedGPass,
	kRenderCatalogDefferedLightPass,
	kRenderCatalogCount,
};

class DrawCall : public LinkList{
public:
	Material* mMaterial;
	VBO* mVBO;
	IBO* mIBO;
	RenderCatalog mCatalog;
	DrawCall();
	void Draw(Camera* camera);
	void ForwardRendering(Camera* camera);
	void DefferedGPass(Camera* camera);
	void DefferedLightPass(Camera* camera);
};
