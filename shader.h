#pragma once
#include "AliceGL.h"
#include "Attribute.h"
#include "UniformUpdater.h"

class Shader {
public:
	GLuint mProgram;
	Attribute* mAttributes;
	UniformUpdater* mUniforms;

	void Init(const char* vs, const char* fs);

	static  Shader* LoadShader(const char* name, const char*vs, const char*fs);

	static std::unordered_map<std::string, Shader*> mCacheShaders;
protected:
	void InitAttributes();
	void InitUniforms();
	void OnDetectUniformMatrix4(GLint location, const char* uniform_name);
	void OnDetectUniformVec4(GLint location, const char* uniform_name);
	void OnDetectUniformSampler2D(GLint location, const char* uniform_name);
	void AppendUniformUpdater(UniformUpdater* uniform_updater);

private:
	std::unordered_map<std::string, MaterialProperty*> mProperty;
};
