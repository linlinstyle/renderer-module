#pragma once
#include "AliceGL.h"
#include "LinkList.h"

enum MaterialUniformPropertyType {
	kMaterialUniformPropertyTypeCameraProjectionMatrix,
	kMaterialUniformPropertyTypeCameraViewMatrix,
	kMaterialUniformPropertyTypeMatrix4,
	kMaterialUniformPropertyVec4,
	kMaterialUniformPropertySampler2D,
	kMaterialUniformPropertyTypeCount,
};

class MaterialProperty{
public:
	MaterialProperty(MaterialUniformPropertyType type, const char* name);
	virtual MaterialProperty* Clone() = 0;
	char mName[64];
	MaterialUniformPropertyType mType;
};

class MaterialPropertyMatrix4 : public MaterialProperty {
public:
	float* mPropertyValue;
	MaterialPropertyMatrix4(const char* name);
	MaterialProperty* Clone();
};

class MaterialPropertyVec4 : public MaterialProperty {
public:
	float* mPropertyValue;
	MaterialPropertyVec4(const char* name);
	MaterialProperty* Clone();
};

class MaterialPropertySampler2D : public MaterialProperty {
public:
	MaterialPropertySampler2D(const char* name);
	GLuint mTextureName; //TextureUint
	MaterialProperty* Clone();
};
