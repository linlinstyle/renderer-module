#pragma once
#include "AliceGL.h"

class LinkList {
public:
	LinkList() :mNext(nullptr) {

	}

	template<typename T>
	T* Next() {
		return (T*)mNext;
	}

	void PushBack(LinkList* node) {
		if (mNext == nullptr) {
			mNext = node;
		}
		else {
			mNext->PushBack(node);
		}
	}

	void Remove(LinkList* node) {
		if (mNext == node) {
			mNext = mNext->mNext;
			node->mNext = nullptr;
		}
		else {
			mNext->Remove(node);
		}
	}

protected:
	LinkList* mNext;
};
