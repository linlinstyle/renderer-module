#pragma once
#include "AliceGL.h"
#include "LinkList.h"

class Attribute : public LinkList{
public:
	GLuint mLocation;
	int mComponentCount;
	unsigned int mBasicDataType;
	bool mbNormalize;
	int mDataStride;
	int mDataOffset;
	Attribute(
		GLuint location,
		int componentCount,
		unsigned int basicDataType,
		bool normalize,
		int dataStride,
		int dataOffset
	);

	void Active();
};
