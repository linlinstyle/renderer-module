#pragma once
#include "AliceGL.h"

struct RenderState {
	bool mbEnableBlend;
	bool mbEnableDepthTest;
	bool mbEnableAlphaTest;
	bool mbWriteR, mbWriteG, mbWriteB, mbWriteA;
	bool mbWriteDepthBuffer;
	unsigned int mSrcBlendFunc, mDstBlendFunc;
	unsigned int mAlphaTestFunction;
	unsigned int mDepthTestFunction;
	float mAlphaTestAlphaValue;
	unsigned int mDrawFace;
	unsigned int mPolygonModel;
	unsigned int mCurrentProgram;
	RenderState();
};

class GlobalRenderState {
public:
	static void Init();

	static void SetBlendState(bool enable);
	static void SetBlendFunc(unsigned int src, unsigned int dst);
	static void SetDepthTestState(bool enable);
	static void SetAlphaTestState(bool enable);
	static void SetColorMask(bool r, bool g, bool b, bool a);
	static void SetDepthMask(bool enable);
	static void SetDepthFunc(unsigned int depthFunc);
	static void SetCurrentProgram(unsigned int program);
	static void SetPolygonMode(unsigned int face, unsigned int mode);
	static void SetAlphaTest(unsigned int func, float ref_value);
protected:
	static RenderState mRenderState;
};
