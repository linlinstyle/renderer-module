#pragma once
#include "AliceGL.h"

enum LightType {
	kLightTypeDirectional,
	kLightTypePoint,
	kLightTypeSpot,
	kLightTypeCount,
};

// : gameObject position -> light position
class Light {
public:
	LightType mType;
	float mAmbient[4], mDiffuse[4], mSpecular[4];
	unsigned int mCullingMask;
	float mSetting0[4], mSetting1[4]; //point radius
	float mPosition[4]; //game object position in real engine

	Light();

public:
	void SetAmbientColor(float r, float g, float b, float a);
	void SetDiffuseColor(float r, float g, float b, float a);
	void SetSpecularColor(float r, float g, float b, float a);
	void SetSetting0(float x, float y, float z, float w);
	void SetSetting1(float x, float y, float z, float w);
	void SetPosition(float x, float y, float z, float w);

	static Light* mMainLight;
	static std::unordered_set<Light*> mLights; //light culling
};
