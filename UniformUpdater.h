#pragma once
#include "AliceGL.h"
#include "LinkList.h"
#include "MaterialProperty.h"

struct Camera {
	glm::mat4 mProjectionMatrix;
	glm::mat4 mViewMatrix;
};

typedef void(*UpdateUniformFunction)(GLuint location, void* camera, void* property_value);

class UniformUpdater : public LinkList{
public:
	UniformUpdater(
		GLuint location,
		UpdateUniformFunction func,
		MaterialProperty* target_peoperty
	);

	UpdateUniformFunction mUpdateFunction;
	MaterialProperty* mTargetProperty;
	GLuint mLocation;

	UniformUpdater* Clone();

	static void UpdateProjectionMatrix4(GLuint location, void* camera, void* property_value);
	static void UpdateViewMatrix4(GLuint location, void* camera, void* property_value);
	static void UpdateMatrix4(GLuint location, void* camera, void* property_value);
	static void UpdateVec4(GLuint location, void* camera, void* property_value);
	static void UpdateSampler2D(GLuint location, void* camera, void* property_value);
};
