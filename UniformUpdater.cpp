#include "UniformUpdater.h"
#include "utils.h"
#include "VertexData.h"
#include "AliceGL.h"

UniformUpdater::UniformUpdater(
	GLuint location, UpdateUniformFunction func, MaterialProperty* target_peoperty)
	:mLocation(location), mUpdateFunction(func), mTargetProperty(target_peoperty) {
}

void UniformUpdater::UpdateProjectionMatrix4(GLuint location, void* camera, void* property_value) {
	glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(((Camera*)camera)->mProjectionMatrix));
}

void UniformUpdater::UpdateViewMatrix4(GLuint location, void* camera, void* property_value) {
	glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(((Camera*)camera)->mViewMatrix));
}

void UniformUpdater::UpdateMatrix4(GLuint location, void* camera, void* property_value) {
	glUniformMatrix4fv(
		location, 1, GL_FALSE,
		((MaterialPropertyMatrix4*)property_value)->mPropertyValue
	);
}

void UniformUpdater::UpdateVec4(GLuint location, void* camera, void* property_value) {
	glUniform4fv(
		location, 1,
		((MaterialPropertyVec4*)property_value)->mPropertyValue
	);
}

void UniformUpdater::UpdateSampler2D(GLuint location, void* camera, void* property_value) {
	glUniform1i(
		location,
		((MaterialPropertySampler2D*)property_value)->mTextureName
	);

}

UniformUpdater* UniformUpdater::Clone() {
	return new UniformUpdater(
		mLocation, mUpdateFunction, 
		mTargetProperty == nullptr ? mTargetProperty : mTargetProperty->Clone()
	);
}
