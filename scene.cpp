#include "scene.h"
#include "utils.h"
#include "shader.h"
#include "framebufferobject.h"
#include "RenderState.h"
#include "Material.h"
#include "GL20/VBO.h"
#include "GL20/IBO.h"
#include "DrawCall.h"
#include "Light.h"
#include "model.h"

struct Rect {
	float mLeft, mBottom, mWidth, mHeight;
	Rect(float l, float b, float w, float h):mLeft(l), mBottom(b), mWidth(w), mHeight(h) {
	}
};

Material* deffered_position_material = nullptr, *deffered_normal_material = nullptr, *deffered_color_material = nullptr;
VBO* vbo = nullptr;
ScreenQuad** quads = nullptr;

Shader* fsqShader = nullptr;
glm::mat4 m, m2;
float color[4] = { 0.74f, 0.78f, 0.68f, 1.0f };
Camera camera;
DrawCall dc;
FrameBufferObject* fbo, *positionFbo, *normalFbo, *colorFbo;

float sViewportWidth = 0.0f, sViewportHeight = 0.0f;

Light* black_light = nullptr;
GLUquadric* point_light_geometry = nullptr, *point_light_disk = nullptr;
int sScreenSplit = 32;

ScreenQuad* InitQuad(float offsetX, float offsetY, float size, int split_count_z = 1) {
	VertexData vertices[4];
	vertices[0].Position[0] = offsetX;
	vertices[0].Position[1] = offsetY;
	vertices[0].Position[2] = 0.0f;
	vertices[0].Position[3] = 1.0f;
	vertices[0].Texcoord[0] = (vertices[0].Position[0] - (-1.0f)) / 2.0f;
	vertices[0].Texcoord[1] = (vertices[0].Position[1] - (-1.0f)) / 2.0f;
	vertices[0].Texcoord[2] = 0.0f;
	vertices[0].Texcoord[3] = 0.0f;

	vertices[1].Position[0] = offsetX + size;
	vertices[1].Position[1] = offsetY;
	vertices[1].Position[2] = 0.0f;
	vertices[1].Position[3] = 1.0f;
	vertices[1].Texcoord[0] = (vertices[1].Position[0] - (-1.0f)) / 2.0f;
	vertices[1].Texcoord[1] = (vertices[1].Position[1] - (-1.0f)) / 2.0f;
	vertices[1].Texcoord[2] = 0.0f;
	vertices[1].Texcoord[3] = 0.0f;

	vertices[2].Position[0] = offsetX;
	vertices[2].Position[1] = offsetY + size;
	vertices[2].Position[2] = 0.0f;
	vertices[2].Position[3] = 1.0f;
	vertices[2].Texcoord[0] = (vertices[2].Position[0] - (-1.0f)) / 2.0f;
	vertices[2].Texcoord[1] = (vertices[2].Position[1] - (-1.0f)) / 2.0f;
	vertices[2].Texcoord[2] = 0.0f;
	vertices[2].Texcoord[3] = 0.0f;

	vertices[3].Position[0] = offsetX + size;
	vertices[3].Position[1] = offsetY + size;
	vertices[3].Position[2] = 0.0f;
	vertices[3].Position[3] = 1.0;
	vertices[3].Texcoord[0] = (vertices[3].Position[0] - (-1.0f)) / 2.0f;
	vertices[3].Texcoord[1] = (vertices[3].Position[1] - (-1.0f)) / 2.0f;
	vertices[3].Texcoord[2] = 0.0f;
	vertices[3].Texcoord[3] = 0.0f;

	ScreenQuad* quad = new ScreenQuad;
	quad->mVBO = new VBO;
	quad->mVBO->SetSize(4);
	quad->mVBO->SubmitData(vertices, sizeof(VertexData) * 4);

	float quad_left = offsetX;
	float quad_bottom = offsetY;
	float quad_width = size;
	float quad_height = size;
	quad->mCommonPlanes[Frustum::kFrustumPlaneLeft].SetABCD(1.0f, 0.0f, 0.0f, -quad_left);
	quad->mCommonPlanes[Frustum::kFrustumPlaneRight].SetABCD(-1.0f, 0.0f, 0.0f, quad_left + quad_width);
	quad->mCommonPlanes[Frustum::kFrustumPlaneBottom].SetABCD(0.0f, 1.0f, 0.0f, -quad_bottom);
	quad->mCommonPlanes[Frustum::kFrustumPlaneTop].SetABCD(0.0f, -1.0f, 0.0f, quad_bottom + quad_height);

	float step = 2.0f / split_count_z;
	for (int i = 0; i < split_count_z; ++i) {
		Frustum* frustum = new Frustum;
		float z_start = i * step;
		float z_end = i * step + step;
		NearFraPlanePair* nf = new NearFraPlanePair;
		nf->mNearPlane.SetABCD(0.0f, 0.0f, 1.0f, 1.0f - z_start);
		nf->mFarPlane.SetABCD(0.0f, 0.0f, -1.0f, -(1.0f - z_end));
		quad->mZSplitPlanes.push_back(nf);
	}
	return quad;
}

bool isOverlappedWith(const Rect& r1, const Rect& r2) {
	if (r1.mLeft > (r2.mLeft + r2.mWidth) || r2.mLeft > (r1.mLeft + r1.mWidth)) {
		return false;
	}
 	if ((r1.mBottom + r1.mHeight) < r2.mBottom || (r2.mBottom + r2.mHeight) < r1.mBottom) {
		return false;
	}
	return true;
}

void Init() {
	point_light_geometry = gluNewQuadric();
	point_light_disk = gluNewQuadric();
	GlobalRenderState::Init();
	Model model;
	model.Init("Res/Sphere.obj");
	vbo = new VBO;
	vbo->SetSize(model.mVertexCount);
	vbo->SubmitData(model.mData, sizeof(VertexData) * model.mVertexCount);

	Light* light = new Light;
	Light::mMainLight = light;
	light->SetDiffuseColor(0.0f, 1.0f, 0.0f, 0.0f);

	Light* point_light = new Light;
	point_light->mType = kLightTypePoint;
	point_light->SetDiffuseColor(0.1f, 0.4f, 0.7f, 1.0f);
	point_light->SetSetting1(1.5f, 0.0f, 0.0f, 0.0f);
	point_light->SetPosition(-1.3f, 0.0f, -3.0f, 1.0f);
	Light::mLights.insert(point_light);

	point_light = new Light;
	point_light->mType = kLightTypePoint;
	point_light->SetDiffuseColor(0.1f, 0.4f, 0.7f, 1.0f);
	point_light->SetSetting1(1.5f, 0.0f, 0.0f, 0.0f);
	point_light->SetPosition(1.3f, 0.0f, -3.0f, 1.0f);
	Light::mLights.insert(point_light);

	black_light = new Light;
	black_light->mType = kLightTypePoint;
	black_light->SetDiffuseColor(0.0f, 0.0f, 0.0f, 1.0f);

	m = glm::translate(0.0f, 0.0f, -3.0f);

	camera.mViewMatrix = glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, -1.0f), glm::vec3(0.0f, 1.0f, 0.0f));

	deffered_position_material = new Material;
	deffered_position_material->mBaseRenderPass = new RenderPass;
	deffered_position_material->mBaseRenderPass->SetShader(Shader::LoadShader("Res/DefferedPosition", "Res/DefferedPosition.vs", "Res/DefferedPosition.fs"));
	deffered_position_material->mBaseRenderPass->SetMatrix4("ModelMatrix", glm::value_ptr(m));
	deffered_position_material->mBaseRenderPass->SetVec4("U_Color", color);

	deffered_normal_material = new Material;
	deffered_normal_material->mBaseRenderPass = new RenderPass;
	deffered_normal_material->mBaseRenderPass->SetShader(Shader::LoadShader("Res/DefferedNormal", "Res/DefferedNormal.vs", "Res/DefferedNormal.fs"));
	deffered_normal_material->mBaseRenderPass->SetMatrix4("ModelMatrix", glm::value_ptr(m));
	deffered_normal_material->mBaseRenderPass->SetVec4("U_Color", color);

	deffered_color_material = new Material;
	deffered_color_material->mBaseRenderPass = new RenderPass;
	deffered_color_material->mBaseRenderPass->SetShader(Shader::LoadShader("Res/DefferedColor", "Res/DefferedColor.vs", "Res/DefferedColor.fs"));
	deffered_color_material->mBaseRenderPass->SetMatrix4("ModelMatrix", glm::value_ptr(m));;
	GLuint texture = CreateTexture2DFromBMP("Res/test.bmp");
	deffered_color_material->mBaseRenderPass->SetSampler2D("U_Texture", texture);

	dc.mVBO = vbo;

	fsqShader = new Shader;
	fsqShader->Init("Res/FullScreenQuad.vs", "Res/texture.fs");
}

glm::vec4 ProjectPointToSceenSpace(Camera& camera, glm::vec4& point_to_projection) {
	glm::vec4 clip_space_pos = camera.mProjectionMatrix * point_to_projection;
	glm::vec4 ndc_space_pos = glm::vec4(
		clip_space_pos.x / clip_space_pos.w,
		clip_space_pos.y / clip_space_pos.w,
		clip_space_pos.z / clip_space_pos.w,
		clip_space_pos.w
	);
	glm::vec4 projected_point(
		ndc_space_pos.x * sViewportWidth / 2.0f, 
		ndc_space_pos.y * sViewportHeight / 2.0f,
		0.0f, 1.0f
	);
	return projected_point;
}

void CaculateScreenSpaceLightGeometry(Light* light, glm::vec4& projected_position, float& projected_radius) {
	glm::vec4 point_light_world_pos(light->mPosition[0], light->mPosition[1], light->mPosition[2], light->mPosition[3]);
	glm::vec4 point_light_view_space_pos = camera.mViewMatrix * point_light_world_pos;
	glm::vec4 point_on_sphere = point_light_view_space_pos + glm::vec4(light->mSetting1[0], 0.0f, 0.0f, 1.0f);
	projected_position = ProjectPointToSceenSpace(camera, point_light_view_space_pos);
	glm::vec4 temp = ProjectPointToSceenSpace(camera, point_on_sphere);
	projected_radius = temp.x - projected_position.x;
}

void SetViewPortSize(float width, float height) {
	sViewportWidth = width;
	sViewportHeight = height;
	camera.mProjectionMatrix = glm::perspective(45.0f, width / height, 0.1f, 1000.0f);

	glm::mat4 world_to_clip_space = camera.mViewMatrix * camera.mProjectionMatrix;

	quads = new ScreenQuad*[sScreenSplit * sScreenSplit];

	float step = 2.0 / float(sScreenSplit);
	for (int y = 0; y < sScreenSplit; ++y) {
		for (int x = 0; x < sScreenSplit; ++x) {
			int quad_index = y * sScreenSplit + x;
			quads[quad_index] = InitQuad(-1.0f + x * step, -1.0f + y * step, step, sScreenSplit);
			ScreenQuad* quad = quads[quad_index];
			ExtractPlane(world_to_clip_space, 0, quad->mCommonPlanes[0].mD, &quad->mCommonPlanes[0]);
			ExtractPlane(world_to_clip_space, 1, quad->mCommonPlanes[1].mD, &quad->mCommonPlanes[1]);
			ExtractPlane(world_to_clip_space, 2, quad->mCommonPlanes[2].mD, &quad->mCommonPlanes[2]);
			ExtractPlane(world_to_clip_space, 3, quad->mCommonPlanes[3].mD, &quad->mCommonPlanes[3]);
			for (int frustum_index = 0; frustum_index < quad->mZSplitPlanes.size(); ++frustum_index) {
				NearFraPlanePair* nf = quad->mZSplitPlanes[frustum_index];
				ExtractPlane(world_to_clip_space, 4, nf->mNearPlane.mD, &nf->mNearPlane);
				ExtractPlane(world_to_clip_space, 5, nf->mFarPlane.mD, &nf->mFarPlane);
			}
		}
	}

	for (int y = 0; y < sScreenSplit; ++y) {
		for (int x = 0; x < sScreenSplit; ++x) {
			int quad_index = y * sScreenSplit + x;
			ScreenQuad* quad = quads[quad_index];
			for (auto iter = Light::mLights.begin(); iter != Light::mLights.end(); ++iter) { //iter lights
				Light * current_light = *iter;
				for (int frustum_index = 0; frustum_index < quad->mZSplitPlanes.size(); ++frustum_index) {
					NearFraPlanePair* nf = quad->mZSplitPlanes[frustum_index];
					Plane frustum[] = {
						quad->mCommonPlanes[0],
						quad->mCommonPlanes[1],
						quad->mCommonPlanes[2],
						quad->mCommonPlanes[3],
						nf->mNearPlane,
						nf->mFarPlane
					};
					if (IntersectSphereFrustumFull(
						glm::vec3(current_light->mPosition[0], current_light->mPosition[1], current_light->mPosition[2]),
						current_light->mSetting1[0],
						frustum
					)) {
						printf("quad(%d:%d) light is in sight!\n", quad_index, frustum_index);
						quad->mLights.insert(current_light);
					}
				}
			}
		}
	}

	for (int y = 0; y < sScreenSplit; ++y) {
		for (int x = 0; x < sScreenSplit; ++x) {
			int quad_index = y * sScreenSplit + x;
			ScreenQuad* quad = quads[quad_index];
			if (quad->mLights.empty()) {
				quad->mLights.insert(black_light);
			}
		}
	}

	fbo = new FrameBufferObject;
	fbo->AttachColorBuffer("color", GL_COLOR_ATTACHMENT0, width, height);
	fbo->AttachDepthBuffer("depth", width, height);

	positionFbo = new FrameBufferObject;
	positionFbo->AttachColorBuffer("color", GL_COLOR_ATTACHMENT0, width, height, GL_RGBA32F);
	positionFbo->AttachDepthBuffer("depth", width, height);
	positionFbo->Finish();

	normalFbo = new FrameBufferObject;
	normalFbo->AttachColorBuffer("color", GL_COLOR_ATTACHMENT0, width, height, GL_RGBA32F);
	normalFbo->AttachDepthBuffer("depth", width, height);
	normalFbo->Finish();

	colorFbo = new FrameBufferObject;
	colorFbo->AttachColorBuffer("color", GL_COLOR_ATTACHMENT0, width, height);
	colorFbo->AttachDepthBuffer("depth", width, height);
	colorFbo->Finish();
}

void TiledRendering() {
	for (int y = 0; y < sScreenSplit; ++y) {
		for (int x = 0; x < sScreenSplit; ++x) {
			int quad_index = y * sScreenSplit + x;
			ScreenQuad* quad = quads[quad_index];
			quad->mVBO->Bind();
			glUseProgram(fsqShader->mProgram);
			GlobalRenderState::SetDepthMask(true);
			GlobalRenderState::SetBlendState(false);
			fsqShader->mAttributes->Active();

			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, positionFbo->GetBuffer("color"));
			glUniform1i(glGetUniformLocation(fsqShader->mProgram, "U_PositionTexture"), 0);

			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, normalFbo->GetBuffer("color"));
			//TODO 发现的问题
			//glUniform1i(glGetUniformLocation(fsqShader->mProgram, "U_NormalTexture"), 1);

			glActiveTexture(GL_TEXTURE2);
			glBindTexture(GL_TEXTURE_2D, colorFbo->GetBuffer("color"));
			glUniform1i(glGetUniformLocation(fsqShader->mProgram, "U_ColorTexture"), 2);

			GlobalRenderState::SetDepthMask(false);
			GlobalRenderState::SetBlendState(true);
			GlobalRenderState::SetBlendFunc(GL_ONE, GL_ONE);
			auto iter = quad->mLights.begin();
			auto iter_end = quad->mLights.end();
			while (iter != iter_end) {
				Light* light = *iter;
				glUniform4fv(glGetUniformLocation(fsqShader->mProgram, "U_LightSetting"), 1, light->mSetting0);
				glUniform4fv(glGetUniformLocation(fsqShader->mProgram, "U_LightSetting1"), 1, light->mSetting1);
				glUniform4fv(glGetUniformLocation(fsqShader->mProgram, "U_LightPos"), 1, light->mPosition);
				glUniform4fv(glGetUniformLocation(fsqShader->mProgram, "U_LightColor"), 1, light->mDiffuse);
				glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
				++iter;
			}
			quad->mVBO->Unbind();
			glUseProgram(0);
		}
	}
}

void PrepareDefferedGPass(Material* material) {
	DrawCall* current = &dc;
	while (current != nullptr) {
		current->mMaterial = material;
		current = current->Next<DrawCall>();
	}
}

void DefferedShading() {
	PrepareDefferedGPass(deffered_position_material);
	positionFbo->Bind();
	//draw call
	dc.DefferedGPass(&camera);
	positionFbo->Unbind();

	PrepareDefferedGPass(deffered_normal_material);
	normalFbo->Bind();
	//draw call
	dc.DefferedGPass(&camera);
	normalFbo->Unbind();

	PrepareDefferedGPass(deffered_color_material);
	colorFbo->Bind();
	dc.DefferedGPass(&camera);
	colorFbo->Unbind();
	TiledRendering();
}

void DebugScene() {
	//3d
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, sViewportWidth / sViewportHeight, 0.1f, 1000.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glColor4ub(255, 255, 255, 255);
	
	for (auto iter = Light::mLights.begin(); iter != Light::mLights.end(); ++iter) {
		Light* light = *iter;
		glPushMatrix();
		glTranslatef(light->mPosition[0], light->mPosition[1], light->mPosition[2]);
		gluSphere(point_light_geometry, light->mSetting1[0], 360, 100);
		glPopMatrix();
	}

	//2d
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(-sViewportWidth / 2.0f, sViewportWidth / 2.0f, -sViewportHeight / 2.0f, sViewportHeight / 2.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glColor4ub(0, 0, 255, 255);
	glBegin(GL_LINES);
	glVertex2f(-sViewportWidth / 2.0f, 0.0f);
	glVertex2f(sViewportWidth / 2.0f, 0.0f);
	glVertex2f(0.0f, -sViewportHeight / 2.0f);
	glVertex2f(0.0f, sViewportHeight / 2.0f);
	glEnd();

	//glBegin(GL_POINTS);
	//glVertex2f(
	//	point_light_opengl_scrren_coordinate.x,
	//	point_light_opengl_scrren_coordinate.y
	//);
	//glVertex2f(
	//	point_on_sphere_opengl_screen_space_coordinate.x,
	//	point_on_sphere_opengl_screen_space_coordinate.y
	//);
	//glEnd();
}

void Draw() {
	float frameTime = GetFrameTime();
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//DebugScene();
	DefferedShading();
}
