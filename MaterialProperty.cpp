#include "MaterialProperty.h"
#include "utils.h"
#include "VertexData.h"
#include "AliceGL.h"

MaterialProperty::MaterialProperty(MaterialUniformPropertyType type, const char* name)
	:mType(type){
	memset(mName, 0, sizeof(mName));
	strcpy(mName, name);
}

MaterialPropertyMatrix4::MaterialPropertyMatrix4(const char* name)
	: MaterialProperty(kMaterialUniformPropertyTypeMatrix4, name), mPropertyValue(nullptr){

}

MaterialProperty* MaterialPropertyMatrix4::Clone() {
	MaterialPropertyMatrix4* ret = new MaterialPropertyMatrix4(mName);
	ret->mPropertyValue = mPropertyValue;
	return ret;
}

MaterialPropertyVec4::MaterialPropertyVec4(const char* name)
	: MaterialProperty(kMaterialUniformPropertyVec4, name), mPropertyValue(nullptr){

}

MaterialProperty* MaterialPropertyVec4::Clone() {
	MaterialPropertyVec4* ret = new MaterialPropertyVec4(mName);
	ret->mPropertyValue = mPropertyValue;
	return ret;
}

MaterialPropertySampler2D::MaterialPropertySampler2D(const char* name) 
	:MaterialProperty(kMaterialUniformPropertySampler2D, name), mTextureName(0){

}

MaterialProperty* MaterialPropertySampler2D::Clone() {
	MaterialPropertySampler2D* ret = new MaterialPropertySampler2D(mName);
	ret->mTextureName = mTextureName;
	return ret;
}
