#pragma once
#include "AliceGL.h"

class Model {
public:
	Model();
	void Init(const char*modelPath);

public:
	void* mData;
	int mVertexCount;
};
